var mongoose = require('mongoose');

module.exports = mongoose.model('projects', {
    name: String,
    category: String,
    description: String,
    imgSrc: String,
    imgSrcModal: String
}, 'projectCollection');