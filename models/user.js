var mongoose = require('mongoose');

module.exports = mongoose.model('users', new mongoose.Schema({
    name: String,
    password: String,
    isAdmin: Boolean
}, { collection: 'usersCollection' }));
