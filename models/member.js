var mongoose = require('mongoose');

module.exports = mongoose.model('members', {
    name: String,
    position: String,
    imgSrc: String
}, 'membersCollection');