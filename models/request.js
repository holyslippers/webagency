var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('requests', new Schema({
    name: String,
    email: String,
    phone: String,
    message: String
}, { collection: 'requestsCollection' }));