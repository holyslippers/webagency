var express = require('express');
var Router = express.Router();

var config = require('../config');
var jwt = require('jsonwebtoken');
var jwtMiddleware = require('../jwtMiddleware');

var User = require('../models/user');
var Request = require('../models/request');

//Actual routes for API
var routes = [
    {
        route: 'members',
        model: './../models/member'
    },
    {
        route: 'projects',
        model: './../models/project'
    },
    {
        route: 'requests',
        model: './../models/request'
    },
    {
        route: 'users',
        model: './../models/user'
    }
];

function isRouteExist(value){
    for(var i in routes){
        if(routes[i].route === value){
            return i;
        }
    }
    return false;
}

//====================Unauthenticated routes====================

//----------site-and-admin----------
Router.get('/', function (req, res) {
    res.sendfile('./public/index.html');
});

Router.get('/terms', function (req, res) {
    res.sendfile('./public/terms.html');
});

Router.get('/privacy', function (req, res) {
    res.sendfile('./public/privacy.html');
});

Router.get('/admin', function (req, res) {
    res.sendfile('./public/admin.html');
});

//----------API-routes----------
Router.get('/api', function (req, res) {
    var list = [];
    for(var i in routes){
        list.push('/api/' + routes[i].route)
    }
    res.send({
        message:'This is webagency RESTful API. Use these routes:',
        routes: list
    });
});

Router.post('/api/auth', function (req, res) {
    User.findOne({
        name: req.body.login
    }, function (err,user) {
        if(err)
        {
            throw err;
        }
        if(!user) {
            res.status(401).send({
                success: false,
                //message for debug purposes
                message: 'Authentication failed. User not found.'
            });
        } else if(user) {
            if(user.password != req.body.password) {
                res.status(401).send({
                    success: false,
                    //message for debug purposes
                    message: 'Authentication failed. Wrong password.'
                });
            } else {
                var token = jwt.sign({
                    name: user.name,
                    isAdmin: user.isAdmin
                    }, req.app.get('jwtSecret'), {
                        expiresIn: '1h' // Token expires in 1 hour
                });
                res.status(200).send({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token
                });
            }
        }
    });
});

Router.get('/api/:resource', function (req, res) {
    if(isRouteExist(req.params.resource) === false) {
        return res.json({success: false, message: 'No such API route exist!'});
    }
    var index = isRouteExist(req.params.resource);
    //даже не знаю насколько это плохая идея
    //если что, то я был молод и наивен
    var Model = require(routes[index].model);
    Model.find(function (err, things) {
        if (err)
            return res.status(500).send(err);
        res.status(201).send(things);
    });
});

Router.post('/api/requests', function (req, res) {
    Request.create({
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        message: req.body.message
    }, function (err, model) {
        if (err)
            res.status(500).send(err);
        Request.find(function (err, things) {
            if (err)
                res.status(500).send(err);
            res.status(201).send(things);
        });
    })
});

//--------------------Authenticated routes--------------------

//JWT auth middleware
//After this all requests must contain valid token
Router.use(jwtMiddleware);

Router.get('/api/user/me', function (req, res) {
    res.status(200).send(req.decoded);
});

Router.post('/api/:resource', function (req, res) {
    if(isRouteExist(req.params.resource) === false) {
        return res.status(400).send({
            success: false,
            message: 'No such API route exist!'
        });
    }
    var index = isRouteExist(req.params.resource);
    var Model = require(routes[index].model);
    Model.create(req.body, function (err, model) {
        if (err)
            res.status(500).send(err);
        Model.find(function (err, things) {
            if (err)
                res.send(err);
            res.status(201).send(things);
        });
    })
});

Router.delete('/api/:resource/:id', function (req, res) {
    if(isRouteExist(req.params.resource) === false) {
        //404
        return res.status(400).send({
            success: false,
            message: 'No such API route exist!'
        });
    }
    var index = isRouteExist(req.params.resource);
    var Model = require(routes[index].model);
    Model.remove({
        _id: req.params.id
    }, function (err,model) {
        if (err)
            res.status(500).send(err);
        Model.find(function (error, things) {
            if (error)
                return res.status(500).send(error);
            res.status(201).send(things);
        });
    });
});

Router.put('/api/:resource/:id', function (req, res) {
    if(isRouteExist(req.params.resource) === false) {
        return res.status(400).send({success: false, message: 'No such API route exist!'});
    }
    var index = isRouteExist(req.params.resource);
    var Model = require(routes[index].model);
    Model.update({
            _id: req.params.id
        }, req.body,

        function (err, smth) {
            if(err)
                res.status(500).send(err);
            Model.find(function (error, things) {
                if (error)
                    res.status(500).send(error);
                res.status(201).send(things);
            });
        });
});

module.exports = Router;

