var jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks expiration date
        jwt.verify(token, req.app.get('jwtSecret'), function(err, decoded) {
            if (err) {
                return res.status(401).send({
                    success: false,
                    valid: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(401).send({
            success: false,
            valid: false,
            message: 'No token provided.'
        });
    }
};