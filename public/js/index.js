//функция для разделения массива на подмассивы
var createGroupedArray = function(arr, chunkSize) {
    var groups = [], i;
    for (i = 0; i < arr.length; i += chunkSize) {
        groups.push(arr.slice(i, i + chunkSize));
    }
    return groups;
};

var app = new Vue({
    el: '#vueApp',
    data: {
        projects: [],
        members: []
    },
    created: function() {
        var self = this;
        $.getJSON('/api/members/', function (data) {
            self.members.push.apply(self.members,data);
        });
        $.getJSON('/api/projects/', function (data) {
            self.projects.push.apply(self.projects,data);
        });
    },
    computed: {
        membersChunks: function() {
            return createGroupedArray(this.members,3);
        },
        projectsChunks: function() {
            return createGroupedArray(this.projects,3);
        }
    }
});

