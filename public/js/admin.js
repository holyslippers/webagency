
Vue.component('grid', {
    template: '#grid-template',
    props: {
        currentUser: Object,
        activeResource: String,
        data: Array,
        columns: Array,
        filterKey: String,
        title: String
    },
    data: function () {
        var sortOrders = {};
        this.columns.forEach(function (key) {
            sortOrders[key] = 1;
        });

        return {
            fillItem: {},
            sortKey: '',
            sortOrders: sortOrders
        }
    },
    computed: {
        filteredData: function () {
            var sortKey = this.sortKey;
            var filterKey = this.filterKey && this.filterKey.toLowerCase();
            var order = this.sortOrders[sortKey] || 1;
            var data = this.data;
            if (filterKey) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1
                    })
                })
            }
            if (sortKey) {
                data = data.slice().sort(function (a, b) {
                    a = a[sortKey];
                    b = b[sortKey];
                    return (a === b ? 0 : a > b ? 1 : -1) * order
                })
            }
            return data
        }
    },
    filters: {
        capitalize: function (str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    },
    methods: {
        sortBy: function (key) {
            this.sortKey = key;
            this.sortOrders[key] = this.sortOrders[key] * -1;
        },
        noId: function (item){
            if(item != '_id'){
                return true;
            } else {
                return false;
            }
        },

        deleteRow: function (item) {
            var self = this;
            if(!this.currentUser.isAdmin){
                return alert('You have not permission to do this!');
            }
            if(self.activeResource == ''){
                return alert('Resource not selected!');
            }
            var deleteRequest = $.ajax({
                type: "DELETE",
                url: self.activeResource + '/' + item._id.toString(),
                data: {
                    "token":localStorage.getItem('id_token')
                },
                success: function (result) {
                    for (var i in self.data) {
                        if (self.data[i]._id === item._id)
                            self.data.splice(i, 1);
                    }
                },
                error: function (err) {
                    alert('Cant delete item with id' + item._id.toString());
                }
            });
        },
        editRow: function (thing) {
            this.fillItem = jQuery.extend(true, {}, thing);
        },
        saveItem: function (item) {
            var self = this;
            if(!this.currentUser.isAdmin){
                return alert('You have not permission to do this!');
            }
            item.token = localStorage.getItem('id_token');
            if(!item._id){
                if(self.activeResource == ''){
                    return alert('Resource not selected!');
                }
                var postRequest = $.ajax({
                    type: "POST",
                    data: item,
                    url: self.activeResource,
                    success: function (result) {
                        self.data = result;
                        $("#input-modal").modal('hide');
                    },
                    error: function (err) {
                        alert('Fail to save item!');
                    }
                });
            } else{
                if(self.activeResource == ''){
                    return alert('Resource not selected!');
                }
                var putRequest = $.ajax({
                    type: "PUT",
                    data: item,
                    url: self.activeResource + '/' + (item._id).toString(),
                    success: function (result) {
                        self.data = result;
                        $("#input-modal").modal('hide');
                    },
                    error: function (err) {
                        //alert('Fail to save item!');
                    }
                });
            }
        }
    }
});

Vue.component('data-select', {
    template: '#data-select',
    props: {
        gridResources: Array,
        getData: Function,
        initialSelect: String,
        logout: Function,
        user: Object
    },
    data: function () {
        return {
            selected: ''
        }
    },
});

var app = new Vue({
    el: '#appVue',
    data: {
        searchQuery: '',
        gridColumns: [],
        gridData: [],
        gridResources: [],
        gridActiveResource: '',
        user: {
            login: '',
            isAdmin: ''
        },
        isUserValid: false
    },
    mounted: function() {
        $('#alert-login').hide();
        this.getData();
    },
    methods: {
        loginUser: function () {                                    //form submit function
            var self = this;
            $.ajax({                                                //get token
                type: "POST",
                url: '/api/auth/',
                data: $("#login-form").serialize()
            }).done( function (result) {
                localStorage.setItem('id_token', result.token);     //if request ok - try get data
                self.getData();
            }).fail(function (err) {
                //Fail to log in alert
                $('#alert-login').slideDown(500);
            });
        },
        logout: function () {                                       //clear all data, init login modal
            localStorage.removeItem('id_token');
            localStorage.removeItem('last_resource');
            this.user.login = '';
            this.gridColumns = [];
            this.gridData = [];
            this.gridResources = [];
            this.gridActiveResource = '';
            $("#login-modal").modal('show');
        },
        checkUser: function (route) {
            var self = this;
            if(localStorage.getItem('id_token') == null){
                self.logout();
                self.isUserValid = false;
            }
            $.ajax({
                type: "GET",
                beforeSend: function(request) {
                    request.setRequestHeader(
                        "x-access-token",
                        localStorage.getItem('id_token')
                    );
                },
                url: '/api/user/me'
            }).done(function (result) {
                //close log in modal
                $("#login-modal").modal('hide');
                self.user.login = result.name;
                self.user.isAdmin = result.isAdmin;
                self.isUserValid = true;
                self.getResources(route);
            }).fail(function (err) {
                self.isUserValid = false;
                self.logout();
            });
        },
        getResources: function (route) {
            var self = this;
            $.ajax({
                type: "GET",
                url: '/api'
            }).done(function (result) {
                self.gridResources = result.routes;
                if(route == undefined){
                    if(localStorage.getItem('last_resource') == undefined) {
                        self.gridActiveResource = self.gridResources[0];
                    } else {
                        self.gridActiveResource = localStorage.getItem('last_resource');
                    }
                }else {
                    self.gridActiveResource = route;
                    localStorage.setItem('last_resource', route);
                }
                self.dataRequest(self.gridActiveResource);
            }).fail(function (err) {
                alert('Cant get resources!');
            });
        },
        getData: function (route) {
            this.checkUser(route)
        },
        dataRequest: function (route) {
            var self = this;
            if(route == undefined){
                return alert('Cant get resources!');
            }
            $.ajax({
                type: "GET",
                url: route,
                success: function (result) {
                    self.gridData = result;
                    self.gridColumns = [];
                    for(var i in self.gridData[0]){
                        self.gridColumns.push(i);
                    }
                    return true;
                },
                error: function (err) {
                    self.gridData = [];
                    self.gridColumns = [];
                    alert('Fail to load data!');
                    return false;
                }
            });
        }
    }
});





