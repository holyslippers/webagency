var express = require('express');
var app = express();
var mongoose = require('mongoose');
var router = require('./routes/router');
var config = require('./config');

var morgan = require('morgan'); 		// log requests to the console
var bodyParser = require('body-parser');

mongoose.connect(config.database);

app.set('jwtSecret',config.secret);
app.use(express.static(__dirname  + '/public'));
app.use(morgan('dev')); 										// log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded
app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use('/', router);

app.listen(3000);
console.log('App listening on port 3000');